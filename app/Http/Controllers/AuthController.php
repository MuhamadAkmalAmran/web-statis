<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('pages.auth');
    }

    public function send(Request $request){
        $firstName = $request->input('firstname');
        $lastName = $request->input('lastname');

        return view('pages.home',['firstName' => $firstName, 'lastName' => $lastName]);
    }
}
