<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\WelcomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [WelcomeController::class,'dashboard']);
Route::get('/auth', [AuthController::class,'register']);
Route::post('/home', [AuthController::class,'send']);
Route::get('/table', function(){
  return view('pages.table');
});

Route::get('/data-table', function(){
    return view('pages.data-table');
  });


// testing master templete
// Route::get('/master', function(){
//   return view('layouts.master');
// });

// Routes cast
// Route::get('/cast', function () {
//     return view('cast.show_cast');
// });
// Route::get('/create', function () {
//     return view('cast.create_cast');
// });
Route::get('/cast',[CastController::class, 'index']);
Route::get('/cast/create',[CastController::class, 'create']);
Route::post('/cast',[CastController::class, 'store']);
Route::get('/cast/{id}',[CastController::class, 'show']);
Route::get('/cast/{id}/edit',[CastController::class, 'edit']);
Route::put('/cast/{id}',[CastController::class, 'update']);
Route::delete('/cast/{id}',[CastController::class, 'destroy']);
