@extends('layouts.master')

@section('title')
Buat Account Baru!
@endsection

@section('content')
<form action="/home" method="post">
    @csrf
    <label for="">First name:</label><br>
    <input type="text" name="firstname"><br><br>
    <label for="">Last Name:</label><br>
    <input type="text" name="lastname"><br><br>
    <label for="">Gender:</label><br> <br>
    <input type="radio" name="Gender" value="Male">Male <br>
    <input type="radio"name="Gender" value="Female">Female <br>
    <input type="radio"name="Gender" value="Other">Other <br><br>
    <label for="">Nationality</label><br><br>
    <select name="nationality" id="">
        <option value="Indonesian">Indonesian</option>
        <option value="English">English</option>
        <option value="Britsh">Britsh</option>
    </select><br><br>
    <label for="">Language</label><br><br>
    <input type="checkbox"name="Language" value="Bahasa Indonesia">Bahasa Indonesia <br>
    <input type="checkbox"name="Language" value="English">English <br>
    <input type="checkbox"name="Language" value="Other">Other <br><br>
    <label for="">Bio</label><br>
    <textarea name="bio" id="" cols="30" rows="10"></textarea><br>
    <input type="submit" value="Sign Up">
</form>
@endsection
