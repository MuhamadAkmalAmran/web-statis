@extends('layouts.master')

@section('title')
    Edit Cast
@endsection

@section('content')
    <form action="/cast/{{$cast->id}}" method="POST">
        @csrf
        @method('put')
        <div class="form-group">
            <label for="nama">Nama</label>
            <input type="text" class="form-control @error('nama') is-invalid @enderror" value="{{$cast->nama}}" name="nama">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="umur">Umur</label>
            <input type="text" class="form-control @error('umur') is-invalid @enderror" value="{{$cast->umur}}" name="umur">
        </div>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="bio">Bio</label>
            <textarea class="form-control @error('bio') is-invalid @enderror" name="bio" rows="3">{{$cast->bio}}</textarea>
        </div>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Update</button>
    </form>
@endsection
